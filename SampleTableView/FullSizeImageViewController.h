//
//  FullSizeImageViewController.h
//  SampleTableView
//
//  Created by Christine Ramos on 5/19/14.
//  Copyright (c) 2014 Stratpoint Technologies, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FullSizeImageViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIImageView *fullImageView;
@property (weak, nonatomic) IBOutlet UILabel *photoLabel;

@property (nonatomic, strong) NSString *imageName;

- (id)initWithImageName:(NSString *)imageName;

@end
