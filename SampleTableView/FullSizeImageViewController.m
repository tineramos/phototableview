//
//  FullSizeImageViewController.m
//  SampleTableView
//
//  Created by Christine Ramos on 5/19/14.
//  Copyright (c) 2014 Stratpoint Technologies, Inc. All rights reserved.
//

#import "FullSizeImageViewController.h"

@interface FullSizeImageViewController ()


@end

@implementation FullSizeImageViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (id)initWithImageName:(NSString *)imageName
{
    self = [super init];
    
    if (self) {
        self.imageName = imageName;
    }
    
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.fullImageView.image = [UIImage imageNamed:self.imageName];
    self.photoLabel.text = self.imageName;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
