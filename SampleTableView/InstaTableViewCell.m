//
//  InstaTableViewCell.m
//  SampleTableView
//
//  Created by Christine Ramos on 5/19/14.
//  Copyright (c) 2014 Stratpoint Technologies, Inc. All rights reserved.
//

#import "InstaTableViewCell.h"

@implementation InstaTableViewCell

- (void)awakeFromNib
{
    // Initialization code
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
    
}

- (void)setCellForData:(NSString *)imageName
{
    self.photo.image = [UIImage imageNamed:imageName];
    self.photoLabel.text = imageName;
}

@end
