//
//  main.m
//  SampleTableView
//
//  Created by Christine Ramos on 5/19/14.
//  Copyright (c) 2014 Stratpoint Technologies, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
