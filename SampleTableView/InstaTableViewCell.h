//
//  InstaTableViewCell.h
//  SampleTableView
//
//  Created by Christine Ramos on 5/19/14.
//  Copyright (c) 2014 Stratpoint Technologies, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface InstaTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *photo;
@property (weak, nonatomic) IBOutlet UILabel *photoLabel;

- (void)setCellForData:(NSString *)imageName;

@end
