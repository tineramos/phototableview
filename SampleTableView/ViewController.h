//
//  ViewController.h
//  SampleTableView
//
//  Created by Christine Ramos on 5/19/14.
//  Copyright (c) 2014 Stratpoint Technologies, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, weak) IBOutlet UITableView *tableview;

@end
