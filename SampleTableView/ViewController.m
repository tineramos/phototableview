//
//  ViewController.m
//  SampleTableView
//
//  Created by Christine Ramos on 5/19/14.
//  Copyright (c) 2014 Stratpoint Technologies, Inc. All rights reserved.
//

#import "ViewController.h"

#import "InstaTableViewCell.h"
#import "FullSizeImageViewController.h"

@interface ViewController ()

@property (nonatomic, strong) NSArray *imageArray;

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    self.tableview.rowHeight = 200;
}

- (NSArray *)imageArray
{
    if (!_imageArray) {
        _imageArray = [NSArray arrayWithObjects:@"cube1", @"cube2", nil];
    }
    return _imageArray;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - DataSource methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.imageArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"cellID";
    
    InstaTableViewCell *cell = (InstaTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (!cell) {
//        cell = [[InstaTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"InstaTableViewCell" owner:self options:nil];
        cell = [nib firstObject];
    }
    
    [cell setCellForData:[self.imageArray objectAtIndex:indexPath.row]];
    
    return cell;
}

#pragma mark - Delegate methods

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    FullSizeImageViewController *fullSizeVC = [[FullSizeImageViewController alloc] initWithImageName:[self.imageArray objectAtIndex:indexPath.row]];
    [self.navigationController pushViewController:fullSizeVC animated:YES];
}

@end
